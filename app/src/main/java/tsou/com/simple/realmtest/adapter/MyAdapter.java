package tsou.com.simple.realmtest.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import tsou.com.simple.realmtest.R;

public class MyAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> mTitles;

    public MyAdapter(Context context, List<String> titles) {
        this.mContext = context;
        this.mTitles = titles;
    }

    @Override
    public int getCount() {
        return mTitles.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = null;
        ViewHolder holder = null;
        if (convertView == null) {
            view = View.inflate(mContext, R.layout.textview_context, null);
            holder = new MyAdapter.ViewHolder(view);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        holder.textview.setText(mTitles.get(position));
        return view;
    }

    class ViewHolder {
        TextView textview;

        ViewHolder(View view) {
            textview = view.findViewById(R.id.textview);
        }
    }
}