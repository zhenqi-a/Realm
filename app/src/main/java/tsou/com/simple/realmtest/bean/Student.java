package tsou.com.simple.realmtest.bean;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Administrator on 2017/12/15 0015.
 * <p>
 * 通过实现 RealmModel接口并添加 @RealmClass修饰符来声明
 * <p>
 * 注意：如果你创建Model并运行过，然后修改了Model。
 * 那么就需要升级数据库，否则会抛异常。
 */
@RealmClass
public class Student implements RealmModel {
    private String name;
    private int num;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
