package tsou.com.simple.realmtest.bean;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by Administrator on 2017/12/19 0019.
 */

public class MigrationPerson extends RealmObject {
    //    @PrimaryKey //取消
    private int id;
    @Required
    private String name;

//    private int age;
    /**
     * 重命名age字段
     */
    private int myage;
    /**
     * 移除sex字段
     */
//    private String sex;

    /**
     * 下面是新增字段
     */
    private int num;
    private String project;
    private Men men;
    private RealmList<Men> mens;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMyage() {
        return myage;
    }

    public void setMyage(int myage) {
        this.myage = myage;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public Men getMen() {
        return men;
    }

    public void setMen(Men men) {
        this.men = men;
    }

    public RealmList<Men> getMens() {
        return mens;
    }

    public void setMens(RealmList<Men> mens) {
        this.mens = mens;
    }
}
