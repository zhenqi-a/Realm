package tsou.com.simple.realmtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.io.Serializable;

import io.realm.Realm;
import tsou.com.simple.realmtest.bean.User;
import tsou.com.simple.realmtest.utils.UIUtils;

public class TestActivity extends AppCompatActivity {

    private TextView mText;
    private Realm mRealm;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        mRealm = UIUtils.getRealmInstance();
        initData();
        initView();
    }

    private void initData() {
        int id = getIntent().getIntExtra("id", -1);
        if (id != -1) {
            user = mRealm.where(User.class).equalTo("id", id).findFirst();
        }
         user = (User) getIntent().getSerializableExtra("user");

    }

    private void initView() {
        mText = (TextView) findViewById(R.id.text);
        if (user != null) {
            mText.setText("id=" + user.getId() +
                    ",name=" + user.getName() + ",age="
                    + user.getAge() + ",sex=" + user.getSex());
        }
    }
}
