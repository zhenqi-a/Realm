package tsou.com.simple.realmtest.bean;

import io.realm.RealmObject;

/**
 * Created by Administrator on 2017/12/15 0015.
 */

public class Person extends RealmObject{
    private String city;
    private String project;
    private Men men;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public Men getMen() {
        return men;
    }

    public void setMen(Men men) {
        this.men = men;
    }
}
