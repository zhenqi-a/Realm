package tsou.com.simple.realmtest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.Random;

import io.realm.Realm;
import tsou.com.simple.realmtest.bean.MigrationPerson;
import tsou.com.simple.realmtest.utils.UIUtils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * 增
     */
    private Button mAdd;
    /**
     * 删
     */
    private Button mDelete;
    /**
     * 改
     */
    private Button mUpdata;
    /**
     * 查
     */
    private Button mCheck;
    /**
     * 数据迁移
     */
    private Button mMigration;
    private Realm mRealm;
    /**
     * 有问题的写法
     */
    private Button mProblem;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRealm != null && !mRealm.isClosed()) {
            mRealm.close();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRealm = UIUtils.getRealmInstance();
        initView();

    }

    private void initView() {
        mAdd = (Button) findViewById(R.id.add);
        mAdd.setOnClickListener(this);
        mDelete = (Button) findViewById(R.id.delete);
        mDelete.setOnClickListener(this);
        mUpdata = (Button) findViewById(R.id.updata);
        mUpdata.setOnClickListener(this);
        mCheck = (Button) findViewById(R.id.check);
        mCheck.setOnClickListener(this);
        mMigration = (Button) findViewById(R.id.migration);
        mMigration.setOnClickListener(this);
        mProblem = (Button) findViewById(R.id.problem);
        mProblem.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.add:
                startActivity(new Intent(this, AddActivity.class));
                break;
            case R.id.delete:
                startActivity(new Intent(this, DeleteActivity.class));
                break;
            case R.id.updata:
                startActivity(new Intent(this, UpdataActivity.class));
                break;
            case R.id.check:
                startActivity(new Intent(this, QueryActivity.class));
                break;
            case R.id.migration://数据迁移前添加数据
//                mRealm.executeTransaction(new Realm.Transaction() {
//                    @Override
//                    public void execute(Realm realm) {
//                        for (int i = 0; i < 10; i++) {
//                            MigrationPerson person = new MigrationPerson();
//                            person.setId(i);
//                            person.setName("huangxiaoguo" + i);
//                            person.setAge(new Random().nextInt(500));
//                            if (i % 2 == 0) {
//                                person.setSex("男");
//                            } else {
//                                person.setSex("女");
//                            }
//                            realm.copyToRealmOrUpdate(person);
//                        }
//
//                    }
//                });
                break;
            case R.id.problem://这样的写法会出错
                startActivity(new Intent(this, ProblemActivity.class));
                break;
        }
    }
}
