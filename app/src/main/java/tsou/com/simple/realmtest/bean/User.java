package tsou.com.simple.realmtest.bean;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Administrator on 2017/12/15 0015.
 * <p>
 * 直接继承于RealmObject来声明 Realm 数据模型
 * <p>
 * 注意：如果你创建Model并运行过，然后修改了Model。
 * 那么就需要升级数据库，否则会抛异常。
 */

public class User extends RealmObject  {
    /**
     * 表示该字段是主键
     * <p>
     * 字段类型必须是字符串（String）或整数（byte，short，int或long）
     * 以及它们的包装类型（Byte,Short, Integer, 或 Long）。不可以存在多个主键，
     * 使用字符串字段作为主键意味着字段被索引（注释@PrimaryKey隐式地设置注释@Index）。
     */
    @PrimaryKey
    private int id;
    /**
     * 表示该字段非空
     * <p>
     * 只能用于Boolean, Byte, Short, Integer, Long, Float, Double, String, byte[] 和 Date。
     * 在其它类型属性上使用 @Required修饰会导致编译失败
     * <p>
     * 注意：基本数据类型不需要使用注解 @Required，因为他们本身就不可为空。
     */
    @Required
    private String name;
    private int age;
    /**
     * 表示忽略该字段
     * <p>
     * 被添加@Ignore标签后，存储数据时会忽略该字段。
     */
    @Ignore
    private String sex;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
