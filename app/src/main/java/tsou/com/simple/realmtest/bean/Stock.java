package tsou.com.simple.realmtest.bean;

import io.realm.RealmObject;

/**
 * Created by Administrator on 2017/12/18 0018.
 * Realm 解析 JSON 时遵循如下规则：
 * <p>
 * 使用包含空值（null）的 JSON 创建对象：
 * 对于非必须（可为空值的属性），设置其值为 null；
 * 对于必须（不可为空值的属性），抛出异常；
 * 使用包含空值（null）的 JSON 更新对象：
 * 对于非必须（可为空值的属性），设置其值为 null；
 * 对于必须（不可为空值的属性），抛出异常；
 * 使用不包含对应属性的 JSON： * 该属性保持不变
 * <p>
 */

public class Stock extends RealmObject {


    /**
     * buyFive : 113700
     * buyFivePri : 8.42
     * buyFour : 87200
     * buyThreePri : 8.35
     * buyTwoPri : 8.36
     * competitivePri : 8.37
     * date : 2012-12-11
     * gid : sh601009
     * increase : 43.99
     * name : 南京银行
     * nowPri : 8.37
     * reservePri : 8.38
     * sellFive : 214535
     * time : 15:03:06
     * todayMax : 8.55
     * traAmount : 290889560
     * traNumber : 34501453
     * yestodEndPri : 8.26
     */

    private String buyFive;
    private String buyFivePri;
    private String buyFour;
    private String buyThreePri;
    private String buyTwoPri;
    private String competitivePri;
    private String date;
    private String gid;
    private String increase;
    private String name;
    private String nowPri;
    private String reservePri;
    private String sellFive;
    private String time;
    private String todayMax;
    private String traAmount;
    private String traNumber;
    private String yestodEndPri;

    public String getBuyFive() {
        return buyFive;
    }

    public void setBuyFive(String buyFive) {
        this.buyFive = buyFive;
    }

    public String getBuyFivePri() {
        return buyFivePri;
    }

    public void setBuyFivePri(String buyFivePri) {
        this.buyFivePri = buyFivePri;
    }

    public String getBuyFour() {
        return buyFour;
    }

    public void setBuyFour(String buyFour) {
        this.buyFour = buyFour;
    }

    public String getBuyThreePri() {
        return buyThreePri;
    }

    public void setBuyThreePri(String buyThreePri) {
        this.buyThreePri = buyThreePri;
    }

    public String getBuyTwoPri() {
        return buyTwoPri;
    }

    public void setBuyTwoPri(String buyTwoPri) {
        this.buyTwoPri = buyTwoPri;
    }

    public String getCompetitivePri() {
        return competitivePri;
    }

    public void setCompetitivePri(String competitivePri) {
        this.competitivePri = competitivePri;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getIncrease() {
        return increase;
    }

    public void setIncrease(String increase) {
        this.increase = increase;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNowPri() {
        return nowPri;
    }

    public void setNowPri(String nowPri) {
        this.nowPri = nowPri;
    }

    public String getReservePri() {
        return reservePri;
    }

    public void setReservePri(String reservePri) {
        this.reservePri = reservePri;
    }

    public String getSellFive() {
        return sellFive;
    }

    public void setSellFive(String sellFive) {
        this.sellFive = sellFive;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTodayMax() {
        return todayMax;
    }

    public void setTodayMax(String todayMax) {
        this.todayMax = todayMax;
    }

    public String getTraAmount() {
        return traAmount;
    }

    public void setTraAmount(String traAmount) {
        this.traAmount = traAmount;
    }

    public String getTraNumber() {
        return traNumber;
    }

    public void setTraNumber(String traNumber) {
        this.traNumber = traNumber;
    }

    public String getYestodEndPri() {
        return yestodEndPri;
    }

    public void setYestodEndPri(String yestodEndPri) {
        this.yestodEndPri = yestodEndPri;
    }
}
