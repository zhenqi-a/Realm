package tsou.com.simple.realmtest.bean;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Administrator on 2017/12/15 0015.
 */

public class MorePerson extends RealmObject {
    private String city;
    private String project;
    private String time;
    private RealmList<Men> mens;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public RealmList<Men> getMens() {
        return mens;
    }

    public void setMens(RealmList<Men> mens) {
        this.mens = mens;
    }
}
